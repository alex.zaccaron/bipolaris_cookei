## *Bipolaris cookei* genome

This repository brings the gene annotation of the genome of the sorghum pathogen *Bipolaris cookei* strain LSLP18.3 (GenBank accession NRSV01000000), published in ["The genome sequence of Bipolaris cookei reveals mechanisms of pathogenesis underlying target leaf spot of sorghum". Scientific reports, 7(1)](https://www.nature.com/articles/s41598-017-17476-x).

Files:
- bipolaris_cookei_annot.gff: gene annotation in gff3 format
- bipolaris_cookei_proteins.fasta: fasta file with the protein sequences
- bipolaris_cookei_func_annot.txt: tab-delimited file with the functional description of the genes (generated with Blast2GO)
